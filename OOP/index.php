<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    include 'animal.php';
    require_once 'ape.php';
    require_once 'frog.php';
	
	$sheep = new Animal("shaun");
    echo 'Name = ' . $sheep->name . '<br>';
	echo 'Legs = ' . $sheep->legs . '<br>';
	echo 'Cold Blooded = ' . $sheep->cold_blooded . '<br>';
	
	$kodok = new Frog("buduk");
	echo '<br>Name = ' . $kodok->name . '<br>';
	echo 'Legs = ' . $kodok->legs . '<br>';
	echo 'Cold Blooded = ' . $kodok->cold_blooded . '<br>';
	echo 'Jump = ' . $kodok->jump . '<br>'; // "hop hop"

	$sungokong = new Ape("kera sakti");
	echo '<br>Name = ' . $sungokong->name . '<br>';
	echo 'Legs = ' . $sungokong->legs . '<br>';
	echo 'Cold Blooded = ' . $sungokong->cold_blooded . '<br>';
	echo 'Yell = ' . $sungokong->yell . '<br>'; // "Auooo"
    ?>
</body>
</html>